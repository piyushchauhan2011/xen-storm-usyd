# Xen Server Automation for creating Storm topology #

Xen Server Automation for creating Storm topology of nimbus, zookeeper, supervisor and workers. It uses python and fabric library to automate the redundant task of creating a topology of Apache Storm. It also automates creation of worker and which is useful in cases of attaching more than one or more workers to the existing topology using single command.

Currently, it requires to have `xenserver` configured properly to `create-images` using automatic copying of public ssh-keys so that we can do `ssh` using python from within the `fabfile.py`. The hostnames and worker names are same as those are the hosts we like to ssh after creation. The IP addresses are discovered using library called `psutil`.

# How to configure xen #
Use command to add copying of ssh public key functionality            ''' already did for current system
```
           mkdir -p /etc/xen-tools/skel/root/.ssh
           chmod -R 700 /etc/xen-tools/skel/root
           cp /root/.ssh/id_rsa.pub /etc/xen-tools/skel/root/.ssh/authorized_keys
           chmod 644 /etc/xen-tools/skel/root/.ssh/authorized_keys
```

Link to follow: http://manpages.ubuntu.com/manpages/precise/man8/xen-create-image.8.html