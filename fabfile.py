from fabric.api import local, run, env, settings

zookeepers = ["zookeeper2"]
workers = ["worker4"]
env.hosts = workers

def ssh_host(hosts):
	env.hosts = hosts
	env.user = "root"
	env.password = ""
	env.key_filename = "/home/piyush/.ssh/id_rsa"

def net_address():
	run("apt-get install -y python-pip")
	run("pip install -U psutil")

def get_address():
	addrs = run("python -c 'import psutil; print(psutil.net_if_addrs()[\"eth0\"][0].address)'")
	print("IP Address: {addrs}".format(addrs=addrs))

def install_java():
	add_repository = """
echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu trusty main" | tee /etc/apt/sources.list.d/webupd8team-java.list
echo "deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu trusty main" | tee -a /etc/apt/sources.list.d/webupd8team-java.list
	"""
	run(add_repository)
	add_key_ring = "apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys EEA14886"
	run(add_key_ring)
	accept_license = """
echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | debconf-set-selections
	"""
	with settings(warn_only=True):
		run(accept_license)
	run("apt-get update")
	oracle_installer = "apt-get install -y oracle-java8-installer"
	run(oracle_installer)

def install_oracle_java():
	wget_java = """
 wget --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u73-b02/jdk-8u73-linux-x64.tar.gz
	"""
	run(wget_java)
	run("tar -zxf jdk-8u73-linux-x64.tar.gz")
	run("mv jdk1.8.0_73 java-8-oracle")
	run("mkdir /usr/lib/jvm")
	run("mv java-8-oracle/ /usr/lib/jvm/")

def install_default_jdk():
	run("apt-get install -y default-jdk")

def append_oracle_java_bash():
	run("echo 'export JAVA_HOME=\"/usr/lib/jvm/java-8-oracle/\"' >> ~/.bashrc")
	run("echo 'export PATH=$PATH:$JAVA_HOME/bin' >> ~/.bashrc")
	run("echo 'export PATH=$PATH:\"/root/apache-storm/bin/\"' >> ~/.bashrc")
	run("source ~/.bashrc")

def append_openjdk_bash():
	run("echo 'export JAVA_HOME=\"/usr/lib/jvm/java-7-openjdk-amd64/\"' >> ~/.bashrc")
	run("echo 'export PATH=$PATH:\"/root/apache-storm/bin/\"' >> ~/.bashrc")
	run("source ~/.bashrc")

def install_libraries():
	run("apt-get update")
	run("apt-get -y upgrade")
	install_usual_library = "apt-get install -y curl wget unzip vim supervisor python-pip libpython-dev"
	run(install_usual_library)

def install_storm():
	get_storm = "wget http://apache.mirror.amaze.com.au/storm/apache-storm-1.0.2/apache-storm-1.0.2.tar.gz"
	run(get_storm)
	extract_storm = "tar -xzf apache-storm-1.0.2.tar.gz && mv apache-storm-1.0.2 apache-storm && cd apache-storm && mkdir data"
	run(extract_storm)

def storm_conf():
	with open("./storm.yaml") as f:
		data = f.read()
		run("echo \"{0}\" > /root/apache-storm/conf/storm.yaml".format(data))

def install_zookeeper():
	run("wget http://apache.mirror.amaze.com.au/zookeeper/zookeeper-3.4.8/zookeeper-3.4.8.tar.gz")
	run("tar -zxf zookeeper-3.4.8.tar.gz && mv zookeeper-3.4.8 zookeeper && cd zookeeper && mkdir data")

def zookeeper_conf():
	with open("./zoo.conf") as f:
		data = f.read()
		run("echo \"{0}\" > /root/zookeeper/conf/zoo.cfg".format(data))

def start_zookeeper():
	run("/root/zookeeper/bin/zkServer.sh start")

def console():
	run("lsb_release")

def supervisor_conf():
	with open("./supervisor.conf") as f:
		data = f.read()
		run("echo \"{0}\" > /etc/supervisor/conf.d/supervisor.conf".format(data))

def restart_supervisor():
	run("service supervisor restart")
	run("supervisorctl status")

def delete_image(worker_name):
	local("sudo xl destroy {hostname}".format(hostname=worker_name))
	local("sudo xen-delete-image --hostname={hostname} --lvm=xenserver-vg-large".format(hostname=worker_name))

def create_image(worker_name):
	create_xen_image = """
sudo xen-create-image \
--hostname={hostname} \
--lvm=xenserver-vg-large \
--dhcp \
--memory=2048MB \
--vcpus=1 \
--pygrub \
--dist=trusty \
--verbose \
--size=15GB \
--genpass=1 \
--install-method=debootstrap
	""".format(hostname=worker_name)

	with settings(warn_only=True):
		local(create_xen_image)

def load_image(worker_name):
	with settings(warn_only=True):
		local("sudo xl create /etc/xen/{hostname}.cfg".format(hostname=worker_name))

def create_zookeeper():
	zookeeper_name = zookeepers[0]
	create_image(zookeeper_name)
	load_image(zookeeper_name)
	ssh_host(zookeepers)
	install_libraries()
	install_java()
	append_oracle_java_bash()
	install_zookeeper()
	zookeeper_conf()
	start_zookeeper()
	net_address()
	get_address()

def create_worker():
	worker_name = workers[0]
	create_image(worker_name)
	load_image(worker_name)
	ssh_host(workers)
	install_libraries()
	install_java()
	#install_default_jdk()
	#install_oracle_java()
	install_storm()
	#append_openjdk_bash()
	append_oracle_java_bash()	
	storm_conf()
	supervisor_conf()
	restart_supervisor()	
	console()
