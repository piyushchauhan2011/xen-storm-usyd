import paramiko
hostname = "worker3"
user = "root"
password = ""
ssh_key = "/home/zzzzzzzzz/.ssh/id_rsa"
ssh_conn = paramiko.SSHClient()
ssh_conn.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh_conn.connect(hostname, username=user, password=password, key_filename=ssh_key)

stdin, stdout, stderr = ssh_conn.exec_command("ls")
print stdout.readlines()
ssh_conn.close()
